extends Node2D

#Resources
const CAMERA_RESOURCE = "res://managers/Camera/Camera.tscn"

#Variables
export(int) var num_players = 2 #Later will be set by some sort of Game Manager
onready var alive_players = num_players
var player_info = []

#Nodes
var camera #in game camera, loaded in ready

#Signals
signal game_over(winner)

#set player information
func set_player_info(all_info):
	for info in all_info:
		self.player_info.append(info)

func _ready():
	var map = get_child(0)

	#Checks if map is valid
	assert(map.get_node("Spawn_Points") != null)
	assert(map.get_node("Players") != null)

	camera = load(CAMERA_RESOURCE).instance()

	var spawn_point_node = map.get_node("Spawn_Points")
	var players_node = map.get_node("Players")

	for i in range(num_players):
		assert(spawn_point_node.get_child(i) != null)
		var info = player_info.pop_front()
		var new_player = spawn_point_node.get_child(i).spawn(players_node, info[0], info[1])
		new_player.connect("died",self,"player_died")
		camera._add_player(new_player)

	self.add_child(camera)

func player_died(square):
	print("%s died!" % square.name)
	alive_players -= 1
	camera._remove_player(square)

	if alive_players == 1:
		print("Game Over")
		#Map -> Players -> Player Alive -> Name
		var player = get_child(0).get_node("Players").get_child(0).name
		emit_signal("game_over", player)