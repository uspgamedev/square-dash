extends Control

# Array de [players_id, cor]
var player_info = []
var total_players = 2 #const for now, will be variable later
var ready_players = 0
var start_ready = false #is the start_warning being displayed?

# Signals
signal players_set(player_info)
signal players_state(ready) #Signal Start_Warning to show or hide

func _input(event):
	if start_ready and event.is_action_pressed("ui_accept"):
		emit_signal("players_set", player_info)

func _on_Player_Selection_player_selected(id, cor):
	assert(id != -1)
	player_info.append([id, cor])
	ready_players += 1

	if total_players == ready_players:
		emit_signal("players_state", true)
		start_ready = true

func _on_Player_Selection_player_unselected(id, cor):
	if id != -1:
		player_info.erase([id, cor])
		ready_players -= 1

	if total_players != ready_players and start_ready:
		emit_signal("players_state", false)
		start_ready = false
