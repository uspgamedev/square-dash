extends KinematicBody2D

#Class used in player select screen
#Mimics normal player controls

#Moving Speed Constants
const NORMAL_SPEED = 300
const DASH_SPEED = 600

#Rotation Constants
const STANDING_ROTATION = 8
const MOVING_ROTATION = 4

#Player ID
export(int) var id = 1

#Movement info
var direction = Vector2(0,0)
var speed = 0 #current speed

#Movement booleans
var is_moving = false

#Rotation booleans
var is_rotating_clock = false
var is_rotating_anti  = false

func _input(event):

	if event.is_action_pressed("player_%s_dash" % id):
		if not is_moving:
			is_moving = true

		speed = DASH_SPEED
		$dash_duration.start()
		direction = Vector2(0,-1).rotated(self.rotation)

	if event.is_action_pressed("player_%s_stop" %id):
		#Stops if the player is moving
		if is_moving:
			is_moving = false
			direction = Vector2(0,0)

	#Spins the player while the rotate keys are being held down, stop when released
	if event.is_action_pressed("player_%s_rotate_clock" % id):
		is_rotating_clock = true
		is_rotating_anti = false
	elif event.is_action_pressed("player_%s_rotate_anti" % id):
		is_rotating_clock = false
		is_rotating_anti = true

	if event.is_action_released("player_%s_rotate_clock" % id):
		is_rotating_clock = false
	elif event.is_action_released("player_%s_rotate_anti" % id):
		is_rotating_anti = false

func _physics_process(delta):

	#Handles movement and collisions
	if is_moving:
		var collision

		collision = move_and_collide(direction*speed*delta)

		#Bounce if the player hits a wall
		if collision != null:
			direction = direction - 2*(direction.dot(collision.normal))*collision.normal
			#makes the other player bounce
			if collision.collider.is_in_group("char_selecter"):
				collision.collider._collided(self.position)

	#Handles rotation
	if is_rotating_clock:
		if not is_moving:
			self.rotate(STANDING_ROTATION*delta)
		else:
			self.rotate(MOVING_ROTATION*delta)
	elif is_rotating_anti:
		if not is_moving:
			self.rotate(-STANDING_ROTATION*delta)
		else:
			self.rotate(-MOVING_ROTATION*delta)

#Dash related functions
func _on_dash_duration_timeout():
	speed = NORMAL_SPEED

#Got hit function
func _collided(from):
	if not is_moving:
		is_moving = true
		speed = NORMAL_SPEED

	direction = (self.position - from).normalized()