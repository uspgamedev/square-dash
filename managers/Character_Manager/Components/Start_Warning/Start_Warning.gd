extends Panel

func _on_Character_Manager_players_state(ready):
	if ready:
		$AnimationPlayer.play("Intro")
	else:
		$AnimationPlayer.play("End")
