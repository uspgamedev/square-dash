extends Panel

export(Color) var cor = Color("8a5959") #bg color
var ready = false #evitar que o resized seja chamado no começo

#Area da colisão 2D
onready var Collision_Area = $Center/Selection_Area/CollisionShape2D.get_shape()

#Selection info
var inside = [] #players who are inside
var who_selected = -1 #who is the winner (gets the skin)
var selected = false

#Signals
signal player_selected(id, cor)
signal player_unselected(id, cor)

func _ready():
	var stylebox = StyleBoxFlat.new()
	stylebox.set_bg_color(cor)
	self.set("custom_styles/panel", stylebox)
	Collision_Area.set_extents(self.get_size()/2)
	ready = true

func _on_Player_Selection_resized():
	if ready:
		Collision_Area.set_extents(self.get_size()/2)

#Area Enter, Exit Funcs
func _on_Selection_Area_body_entered(body):
	if body.is_in_group("char_selecter"):
		inside.append(body.id)
		handle_selection()

func _on_Selection_Area_body_exited(body):
	if body.is_in_group("char_selecter"):
		inside.erase(body.id)
		handle_selection()

func handle_selection():
	if inside.size() != 1:
		selected = false
		emit_signal("player_unselected", who_selected, cor)
		who_selected = -1

	else:
		selected = true
		who_selected = inside[0]
		emit_signal("player_selected", who_selected, cor)
