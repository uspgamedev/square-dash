extends Control

#Constants
const MAP_SELECT_SCENE = preload("res://managers/Map_Manager/Map_Manager.tscn")
const GAMEPLAY_SCENE = preload("res://managers/Gameplay_Manager/Gameplay_Manager.tscn")
const DEFAULT_MAP = "res://maps/real_maps/Test_Map/Test_Map.tscn" #used only if map_list is empty

#Variables

##Player Info
var player_info = []

##Map Variables
var map_list = [] #The maps that will be played
var map_shuffle = false #should the map list be shuffled?

#Signals

#Funcions

##When all players selected their skin
func _players_set(all_info):
	for info in all_info:
		self.player_info.append(info)
	go_to_map_selection()

## Used to go from player_selection to map_selection
func go_to_map_selection():
	#Delete the old scene
	get_child(0).queue_free()

	#Add the new scene and connect the signals
	var map_select_node = MAP_SELECT_SCENE.instance()
	map_select_node.connect("selection_finished", self, "_maps_selected")

	self.add_child(map_select_node)

## Called when map_selection is finished
func _maps_selected(map_list):
	self.map_list = map_list
	go_to_gameplay()

## Used to go from map_selection to gameplay or change maps
func go_to_gameplay():
	if map_list.empty():
		#map list is empty: go back to map selection
		go_to_map_selection()
		return
	#Delete the old scene
	get_child(0).queue_free()
	#Add the new scene and add the map to it
	var gameplay_node = GAMEPLAY_SCENE.instance()
	gameplay_node.set_player_info(player_info)
	gameplay_node.connect("game_over", self, "_game_finished")

	if map_shuffle == true: map_list.shuffle()

	var map_node = load(map_list.pop_front()).instance() #remove from the list and instance the map

	#Instance the gameplay_scene with map
	gameplay_node.add_child(map_node)

	#We must use call_deferred in order to
	#avoid multiple _game_finished calls
	self.call_deferred("add_child", gameplay_node)

## Called when a match is over
func _game_finished(winner):
	print("%s won!" % winner) #save the score somewhere else later
	go_to_gameplay()
