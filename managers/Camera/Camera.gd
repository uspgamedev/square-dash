extends Camera2D

const PADDING = 100

#variables
var players_node = []

func _add_player(player):
	assert(not players_node.has(player))
	self.players_node.append(player)

func _remove_player(player):
	assert(players_node.has(player))
	self.players_node.remove(players_node.find(player))

func _process(delta):
	if players_node.size() > 0:
		var sum_pos = Vector2(0,0)
		var max_pos = Vector2(-INF, -INF)
		var min_pos = Vector2(INF, INF)

		#"Cheap" way to get the player center is
		#to calculate the median

		#Sum all players positions
		for player in players_node:
			var player_pos = player.get_global_position()
			sum_pos += player_pos

			if player_pos.x > max_pos.x:
				max_pos.x = player_pos.x

			if player_pos.x < min_pos.x:
				min_pos.x = player_pos.x

			if player_pos.y > max_pos.y:
				max_pos.y = player_pos.y

			if player_pos.y < min_pos.y:
				min_pos.y = player_pos.y

		#Value used to correct padding
		var correct_pixel = (get_viewport().size/100) * PADDING

		#Calculate Position
		sum_pos /= players_node.size()

		#Calculate Zoom
		var diff_pos = max_pos - min_pos
		var diff_zoom = Vector2((diff_pos.x + 2*correct_pixel.y)/get_viewport().size.x,
								(diff_pos.y + 2*correct_pixel.y)/get_viewport().size.y)

		var new_zoom = max( max(diff_zoom.x, diff_zoom.y), 1 )

		self.global_position = sum_pos
		self.zoom = Vector2(new_zoom,new_zoom)