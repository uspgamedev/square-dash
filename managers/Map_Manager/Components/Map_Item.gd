extends Control

#Variables
var selected = false

## Map Info
var map_path = "res://maps/real_maps/Test_Map/Test_Map.tscn"
var map_icon = "res://managers/Map_Manager/Components/icon.png"
var map_name = "Example Map Name"

#Signals
signal set_selected(map)
signal set_unselected(map)

func setup(map_path, map_name, map_icon):
	if map_path != null:
		self.map_path = map_path
		self.map_name = map_name

	if map_icon != null:
		self.map_icon = map_icon

func _ready():
	$HSplitContainer/Map_Name.text = map_name
	$HSplitContainer/Map_Icon.texture = load(map_icon)

func _gui_input(event):
	if event.is_action_pressed("ui_accept"):
		if selected:
			$Selected_BG.hide()
			emit_signal("set_unselected", map_path)
		else:
			$Selected_BG.show()
			emit_signal("set_selected", map_path)
		selected = !selected
