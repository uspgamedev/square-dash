extends Control

#Constants
const MAP_PATH = "res://maps/real_maps"
const MAP_LIST_RESOURCE = preload("res://managers/Map_Manager/Components/Map_Item.tscn")

#Variables
var map_list = []

#Signals
signal selection_finished(map_list)

#Functions
func _ready():
	#makes sure the map manager is not the root
	#(should be game manager)
	#assert(get_tree().get_root().get_child(0) != self)
	#Since it works with signals now, it might not be needed

	var dir = Directory.new()

	#Start to search the directory
	assert(dir.open(MAP_PATH) == OK)
	dir.list_dir_begin(true,true)
	var file_name = dir.get_next()

	#Get all folder inside MAP_PATH
	while (file_name != ""):
		if dir.current_is_dir():
			print("Found directory: " + file_name)
			var map_icon
			var map_name
			var map_path

			#Open the folder we just found
			var map_folder = Directory.new()
			assert(map_folder.open("%s/%s" % [MAP_PATH, file_name] ) == OK)
			map_folder.list_dir_begin(true,true)

			var map_file_name = map_folder.get_next()
			while(map_file_name != ""):

				#Find the .tscn and .png files inside the map folder
				if map_file_name.match("*.tscn"):
					map_name = map_file_name
					map_path = "%s/%s/%s" % [MAP_PATH, file_name, map_file_name]
				elif map_file_name.match("*.png"):
					map_icon = "%s/%s/%s" % [MAP_PATH, file_name, map_file_name]

				map_file_name = map_folder.get_next()

			map_folder.list_dir_end()

			#Create the new map_list_item
			var map_list_item = MAP_LIST_RESOURCE.instance()
			map_list_item.setup(map_path, map_name, map_icon)
			$Map_List/List.add_child(map_list_item)
			#Connect the signals
			map_list_item.connect("set_selected", self, "_add_map")
			map_list_item.connect("set_unselected", self, "_remove_map")

		file_name = dir.get_next()

	dir.list_dir_end()

#Called by Map_Items
func _add_map(map):
	assert(not map_list.has(map))
	map_list.append(map)

func _remove_map(map):
	assert(map_list.has(map))
	map_list.remove(map_list.find(map))

#Called by the "Begin" button
func _on_Begin_pressed():
	emit_signal("selection_finished", self.map_list)
