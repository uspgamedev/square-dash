extends KinematicBody2D

#Moving Speed Constants
const NORMAL_SPEED = 300
const DASH_SPEED = 600
const SUPER_DASH_SPEED = 3000
const RECOIL_SPEED = 500

#Rotation Constants
const STANDING_ROTATION = 8
const MOVING_ROTATION = 4

#Player ID
export(int) var id = 1

#Attributes
var health = 3
var energy = 1

#Movement info
var direction = Vector2(0,0)
var speed = 0 setget ,get_speed #current speed

#Movement booleans
var is_in_recoil = false #recoil when the square is hit
var is_invincible = false #get invencibility when damage is taken
var is_charging = false
var is_moving = false
var can_teleport = true #without this, the square gets stuck continuously teleporting

#Rotation booleans
var is_rotating_clock = false
var is_rotating_anti  = false

#Signals
signal died(square)

#Called when spawned
func _set_skin(cor):
	$body.set_color(cor)

#Getters/Setters

func get_speed():
	return speed

func _ready():
	#matches the side's id to the square so it doesn't hurt itself
	$body/front.id = self.id
	$body/front.body = self
	$body/left.id = self.id
	$body/back.id = self.id
	$body/right.id = self.id

func _input(event):

	#The player may not dash (both normal and super) while charging a super dash
	if not is_charging:
		if event.is_action_pressed("player_%s_dash" % id) and not is_in_recoil and energy > 0:
			if not is_moving:
				is_moving = true

			remove_energy()
			speed = DASH_SPEED
			$dash_duration.start()
			direction = Vector2(0,-1).rotated(self.rotation)

		if event.is_action_pressed("player_%s_stop" %id) and not is_in_recoil and energy > 0:
			#Stops if the player is moving
			if is_moving:
				is_moving = false
				direction = Vector2(0,0)
			#Performs a "super dash" if the player is not moving
			else:
				is_charging = true
				$super_dash_windup.start()
				self.modulate = Color("ffff00")
			remove_energy()

	#Spins the player while the rotate keys are being held down, stop when released
	if event.is_action_pressed("player_%s_rotate_clock" % id):
		is_rotating_clock = true
		is_rotating_anti = false
	elif event.is_action_pressed("player_%s_rotate_anti" % id):
		is_rotating_clock = false
		is_rotating_anti = true

	if event.is_action_released("player_%s_rotate_clock" % id):
		is_rotating_clock = false
	elif event.is_action_released("player_%s_rotate_anti" % id):
		is_rotating_anti = false

func _physics_process(delta):

	#Handles movement and collisions
	if is_moving:
		var collision

		collision = move_and_collide(direction*speed*delta)

		#Bounce if the player hits a wall
		if collision != null:
			if collision.collider.is_in_group("wall") or collision.collider.is_in_group("square"):
				direction = direction - 2*(direction.dot(collision.normal))*collision.normal

	#Handles rotation
	if is_rotating_clock:
		if not is_moving:
			self.rotate(STANDING_ROTATION*delta)
		else:
			self.rotate(MOVING_ROTATION*delta)
	elif is_rotating_anti:
		if not is_moving:
			self.rotate(-STANDING_ROTATION*delta)
		else:
			self.rotate(-MOVING_ROTATION*delta)

#Dash related functions
func _on_dash_duration_timeout():
	speed = NORMAL_SPEED

func _on_super_dash_windup_timeout():
	is_charging = false
	is_moving = true

	speed = SUPER_DASH_SPEED
	self.modulate = Color("ffffff")
	$dash_duration.start()
	direction = Vector2(0,-1).rotated(self.rotation)

#Teleport functions (called by teleports)
func _teleported():
	can_teleport = false
	$teleport_timer.start()

func _on_teleport_timer_timeout():
	can_teleport = true

#Energy related functions
#Right now, energy is represented within the sides of the squares (left, back and right).
func _on_energy_recharge_timeout():
	$body/back.refresh()
	energy += 1

func remove_energy():
	$body/back.exhaust()
	energy -= 1
	$energy_recharge.start()

#Damage and recoil functions
func _on_recoil_duration_timeout():
	is_in_recoil = false
	is_invincible = false
	speed = NORMAL_SPEED
	self.modulate = Color("ffffff")

func _on_hit_dealt(position, e_speed): #if the player hits something, he bounces
	is_in_recoil = true
	is_moving = true

	var speed_dif = 1
	
	if e_speed != -1:
		speed_dif = min( 3, abs(self.speed - e_speed)/NORMAL_SPEED + 1 )

	speed = RECOIL_SPEED * speed_dif
	$recoil_duration.start()

	direction = (self.position - position).normalized()

func _on_hit_taken(position, e_speed, side, shattered): #if the player is hit, he is launched away
	if not is_invincible: #the player may not be hit if he's already in recoil
		direction = (self.position - position).normalized()

		is_in_recoil = true
		is_moving = true

		var speed_dif = 1
		if e_speed != -1:
			speed_dif = min( 3, abs(self.speed - e_speed)/NORMAL_SPEED + 1 )

		speed = RECOIL_SPEED * speed_dif
		$recoil_duration.start()

		if not is_invincible and not shattered: #Only receives damage if the side hit was not hit before
			health -= 1
			side.shatter()
			if health == 0:
				self.die()

			self.modulate = Color("0000ff")
			self.is_invincible = true

			if is_charging: #stop charging if hit while charging
				is_charging = false
				$super_dash_windup.stop()


func die(): #big F
	emit_signal("died",self)
	queue_free()
