extends Area2D

var id = 0 #should match and is set by the square body
var body = null #father's node (set by father)

signal hit(position, speed) #send this whenever you hit something (that can be hurt)

#Returns the square's current speed
func get_speed():
	return body.get_speed()

func _on_front_area_entered(area):
	var speed = -1 #hit speed
	if (area.is_in_group("hurtbox") or area.is_in_group("hitbox")) and area.id != self.id:
		if area.is_in_group("hitbox"):
			speed = area.get_speed()
		emit_signal("hit", area.global_position, speed)
	elif area.is_in_group("hazard"): #hazards don't have ids
		emit_signal("hit", area.global_position, speed)