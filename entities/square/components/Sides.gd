extends Area2D

var id = 0 #should match and is set by the square body

var shattered = false #whether this side was hit before or not

signal hit(position, speed, side, shattered) #send this whenever the side is hit

#Hit related functions
func _on_sides_area_entered(area):
	var speed = -1
	if area.is_in_group("hitbox") and area.id != self.id: #make sure not to hit yourself
		speed = area.get_speed()
		emit_signal("hit", area.global_position, speed, self, shattered)
	elif area.is_in_group("hazard"): #hazards don't have ids
		emit_signal("hit", area.global_position, speed, self, shattered)

func shatter(): #Called by the square, since it might be on recoil when hit
	shattered = true
	$crack.visible = true

#Energy related functions
func exhaust(): #energy lost
	$shape.color = Color(0.5,0.5,0.5)

func refresh(): #energy adquired
	$shape.color = Color(1,1,1)