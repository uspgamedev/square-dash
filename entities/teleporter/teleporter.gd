extends Area2D

#emit this whenever the player should be teleported
signal teleports(player)

#checks if the player entered the portal
func _on_teleporter_body_entered(body):
	if body.is_in_group("square") and body.can_teleport:
		body._teleported()
		emit_signal("teleports",body)

#connect the "teleports" signal from a different portal to this function
func teleport(who):
	who.position = self.position