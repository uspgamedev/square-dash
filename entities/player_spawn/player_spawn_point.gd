extends Node2D

#the players node path
const PLAYER_RESOURCE = preload("res://entities/square/Square.tscn")

func _ready():
	self.hide()

#Called (normally) by gameplay_manager
func spawn(where, player_id, color):
	var new_player = PLAYER_RESOURCE.instance()

	#Creates the new player
	new_player.id = player_id
	new_player.position = self.position
	new_player._set_skin(color)

	#Adds the new player to the scene
	where.add_child(new_player)

	#return pointer
	return new_player