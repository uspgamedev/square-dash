extends StaticBody2D

export(float) var speed = 1
export(bool) var inverted = false

func _ready():
	if inverted:
		speed = -speed

func _physics_process(delta):
	self.rotate(speed*delta)

